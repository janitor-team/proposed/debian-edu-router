Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Debian Edu Router
Upstream-Contact: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
Source: https://salsa.debian.org/debian-edu/debian-edu-router/

Files: AUTHORS
 ChangeLog
 Makefile.debug
 Makefile.iso
 README.md
 conf/apache2/conf-available/debian-edu-router-deployserver.conf
 conf/debian-edu/fai/debian-edu-router-fai.TEMPLATE/NFSROOT
 conf/debian-edu/fai/debian-edu-router-fai.TEMPLATE/apt/sources.list.in
 conf/debian-edu/router.conf
 conf/debian-edu/router.conf.d/10_debian-edu-router-config
 debian/changelog
 debian/copyright.in
 debian/copyright
 debian/control
 debian/debian-edu-router-config.install
 debian/debian-edu-router-config.lintian-overrides
 debian/debian-edu-router-config.templates
 debian/debian-edu-router-deployserver.dirs
 debian/debian-edu-router-deployserver.install
 debian/debian-edu-router-deployserver.lintian-overrides
 debian/debian-edu-router-fai.dirs
 debian/debian-edu-router-fai.docs
 debian/debian-edu-router-fai.install
 debian/debian-edu-router-fai.lintian-overrides
 debian/salsa-ci.yml
 debian/source/format
 debian/source/lintian-overrides
 fai/config/_obsolete-files.d/debian-edu-router-fai.removed
 fai/config/class/DEBIAN.var
 fai/config/class/DEBIAN_11.var
 fai/config/class/DEBIAN_12.var
 fai/config/class/DEBIAN_bookworm.var
 fai/config/class/DEBIAN_bullseye.var
 fai/config/class/FAIBASE.var
 fai/config/class/LANG_FRBELGIAN.var
 fai/config/class/LANG_GERMAN.var
 fai/config/class/z20_debian-edu-router.profile
 fai/config/debconf/DEBIAN
 fai/config/debconf/DEBIAN_EDU_ROUTER
 fai/config/debconf/GATEWAY
 fai/config/debconf/LANG_FRBELGIAN
 fai/config/debconf/LANG_GERMAN
 fai/config/disk_config/FAIBASE
 fai/config/disk_config/FAIBASE_EFI
 fai/config/disk_config/LVM_EDU_ROUTER
 fai/config/files/etc/apt/sources.list.d/debian-edu-router-development.list/DEVELOPMENT
 fai/config/files/etc/apt/sources.list/GATEWAY
 fai/config/files/etc/fai/apt/sources.list/DEBIAN
 fai/config/files/etc/fai/fai.conf/GATEWAY.in
 fai/config/files/etc/hosts/GATEWAY
 fai/config/files/etc/motd/DEBIAN_EDU_ROUTER
 fai/config/files/etc/motd/FAIBASE
 fai/config/files/etc/squid/conf.d/debian-edu-router.conf/PROXY
 fai/config/hooks/instsoft.DEBIAN.sh
 fai/config/package_config/DEBIAN
 fai/config/package_config/DEBIAN_EDU_ROUTER
 fai/config/package_config/DEVELOPMENT.gpg
 fai/config/package_config/FAIBASE
 fai/config/package_config/FIREWALL
 fai/config/package_config/GATEWAY
 fai/config/package_config/LANG_FRBELGIAN
 fai/config/package_config/LANG_GERMAN
 fai/config/package_config/NTP_SERVER
 fai/config/package_config/PROXY
 fai/config/package_config/WEBFILTER
Copyright: 2001-2022, Thomas Lange <lange@informatik.uni-koeln.de>
  2011-2018, Andreas B. Mundt <andi@debian.org>
  2022-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2022-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
License: GPL-3+
Comment:
 Some of these files have been derived from official FAI
 (GPL-2+) config space example, some from Debian LAN (GPL-3+)
 and several have been added by the Debian Edu Router maintainers.
 Thus, let's simply assume GPL-3+ here (as the files don't ship
 license headers.
 .
 As copyright holders, let's assume major contributors of
 FAI, Debian LAN and Debian Edu Router itself.

Files: bin/debian-edu-router-fai_install
 bin/debian-edu-router-fai_mkcd
 bin/debian-edu-router-fai_mkconfigspacetarball
 bin/debian-edu-router-fai_softupdate
 bin/debian-edu-router-fai_updateconfigspace
 conf/debian-edu/debian-edu-router-fai.conf
 conf/profile.d/d-e-r_on-login.sh
 debian/rules
 fai/config/class/51-os-version-from-nfsroot
 fai/config/class/90-development
 fai/config/class/GATEWAY.var
 fai/config/hooks/install.DEBIAN_EDU_ROUTER.sh
 fai/config/hooks/install.GATEWAY.sh
 fai/config/scripts/DEBIAN_EDU_ROUTER/10-networking
 fai/config/scripts/GATEWAY/10-networking
 fai/config/scripts/LAST/90-reconfigure-d-e-r-config
Copyright: 2010-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
  2022-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2022-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
License: GPL-2+

Files: conf/debian-edu/fai/debian-edu-router-fai.TEMPLATE/fai.conf.in
 conf/debian-edu/fai/debian-edu-router-fai.TEMPLATE/grub.cfg
 conf/debian-edu/fai/debian-edu-router-fai.TEMPLATE/nfsroot.conf.in
 fai/config/class/01-classes
 fai/config/class/10-base-classes
 fai/config/class/20-hwdetect.sh
 fai/config/class/40-parse-profiles.sh
 fai/config/class/41-warning.sh
 fai/config/class/60-misc
 fai/config/class/85-efi-classes
 fai/config/hooks/instsoft.GATEWAY.sh
 fai/config/hooks/savelog.LAST.sh
 fai/config/hooks/setup.DEFAULT.sh
Copyright: 2001-2022, Thomas Lange <lange@informatik.uni-koeln.de>
  2002-2013, Thomas Lange <lange@informatik.uni-koeln.de>
  2015, Thomas Lange <lange@informatik.uni-koeln.de>
  2022-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2022-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
License: GPL-2+

Files: fai/config/scripts/DEBIAN/10-rootpw
 fai/config/scripts/DEBIAN/20-capabilities
 fai/config/scripts/FAIBASE/10-misc
 fai/config/scripts/FAIBASE/20-removable_media
 fai/config/scripts/GRUB_EFI/10-setup
 fai/config/scripts/GRUB_PC/10-setup
 fai/config/scripts/LAST/50-misc
 fai/config/tests/FAIBASE_TEST
Copyright: 2001-2012, Thomas Lange <lange@informatik.uni-koeln.de>
  2001-2016, Thomas Lange <lange@informatik.uni-koeln.de>
  2001-2022, Thomas Lange <lange@informatik.uni-koeln.de>
  2006, 2009, Thomas Lange <lange@informatik.uni-koeln.de>
License: GPL-2+

Files: bin/debian-edu-router-loginmenu.sh
 debian/debian-edu-router-config.config
 debian/debian-edu-router-config.postinst
 debian/debian-edu-router-config.postrm
 debian/debian-edu-router-fai.prerm
Copyright: 2010-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
  2022-2023, 2023 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
  2022-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: fai/config/hooks/defvar.DEFAULT.sh
 fai/config/hooks/install.DEFAULT.sh
 fai/config/hooks/tests.GATEWAY.sh
Copyright: 2011-2018, Andreas B. Mundt <andi@debian.org>
  2022-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2022-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
License: GPL-3+

Files: update-po.sh
 update-pot.sh
Copyright: 2017, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

Files: fai/config/scripts/DEBIAN/30-interface
 fai/config/scripts/PROXY/20-squid-wpad
Copyright: 2001-2022, Thomas Lange <lange@informatik.uni-koeln.de>
  2011-2018, Andreas B. Mundt <andi@debian.org>
  2022-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2022-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
License: GPL-3+

Files: debian/debian-edu-router-config.common
 fai/config/scripts/GATEWAY/20-startup-shutdown-chiptune
Copyright: 2022-2023, Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
  2022-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
  2023, Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
  2023, Pädagogisches Landesinstitut Rheinland-Pfalz
License: GPL-2+

Files: fai/config/tests/Faitest.pm
Copyright: 2008, Sebastian Hetze
  2009, Thomas Lange <lange@informatik.uni-koeln.de>
License: GPL-2+

Files: fai/config/scripts/DEBIAN/40-misc
Copyright: 2001-2016, Thomas Lange <lange@informatik.uni-koeln.de>
  2010-2011, Michael Goetze <mgoetze@mgoetze.net>
  2022-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2022-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
License: GPL-3+

Files: fai/config/files/lib/systemd/system/startup-shutdown-chiptune.service/GATEWAY
Copyright: 2023, Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
License: GPL-2+

Files: po/*
  debian/po/*
Copyright: 2022-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2023, Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
  2022-2023, Pädagogisches Landesinstitut Rheinland-Pfalz
License: GPL-2+
Comment:
 Using YEAR, COPYRIGHT HOLDER pattern here.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".
