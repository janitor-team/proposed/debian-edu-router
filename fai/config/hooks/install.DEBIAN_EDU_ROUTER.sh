#! /bin/bash

set -e

# Copyright (C) 2022-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
#   Abort installation if there are not enough network interfaces
#   available in the host.

# this script is localizable
export TEXTDOMAIN="debian-edu-router-fai"
. gettext.sh

red=$(mktemp)
echo 'screen_color = (CYAN,RED,ON)' > $red

COUNT_NICS="$(ip link show | grep -E "^\w+:" | cut -d ":" -f2 | grep -v lo | sed -e 's/\s//g' | wc -w)"

if [ ${COUNT_NICS} -le 1 ]; then
	DIALOGRC=${red} \
	dialog --stdout \
	       --title " $(eval_gettext "Not enough Network Interfaces") " \
	       --backtitle " $(eval_gettext "Configure Base System") " \
	       --msgbox "\n$(eval_gettext "There are not enough network interfaces available on this system.")\n\n$(eval_gettext "Number of interfaces required:") 2\n$(eval_gettext "Number of interfaces found: ${COUNT_NICS}")\n" 12 45

	reset
	echo
	echo "hooks/install.DEBIAN_EDU_ROUTER.sh: Not enough network cards. Cancelling installation..."
	echo
	exit 1

fi

set +e
