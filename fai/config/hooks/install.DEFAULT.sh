#! /bin/bash

set -e

# Copyright (C) 2011-2018 Andreas B. Mundt <andi@debian.org>
# Copyright (C) 2022-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
#   Prompt for the root password if $ROOTPW is empty.
#
#   Set env variable TEST_MODE to some value if you want to test the dialogs of
#   this script as a developer.

# this script is localizable
export TEXTDOMAIN="debian-edu-router-fai"
. gettext.sh

red=$(mktemp)
echo 'screen_color = (CYAN,RED,ON)' > $red

if [ -n "${ROOTPW}" ] && [ -z "${TEST_MODE}" ]; then
	echo "Root password hash is set."
else
	while [ -z "$ROOTPW" ] ; do
		if ! { inp1=`dialog --insecure --stdout --title " $(eval_gettext "Password for super-user 'root'") " --backtitle " $(eval_gettext "Configure Base System") " --passwordbox \
		       "\n$(eval_gettext "Please provide a secure password for the super-user 'root' (system administrator account).")\n\n$(eval_gettext "Please enter the root password:")" 12 78` && \
		       inp2=`dialog --insecure --stdout --title " $(eval_gettext "Password for super-user 'root' [again]") " --backtitle " $(eval_gettext "Configure Base System") " --passwordbox \
		       "\n$(eval_gettext "Please re-enter the same password for the super-user 'root' account.")\n\n$(eval_gettext "Please enter the root password again:")" 12 78`; }; then

			if { ! dialog --stdout \
			              --yes-button "$(eval_gettext "Yes")" \
			              --no-button "$(eval_gettext "Cancel installation")" \
			              --title " $(eval_gettext "Password for super-user 'root'") " \
			              --backtitle " $(eval_gettext "Configure Base System") " \
			              --yesno "\n$(eval_gettext "Entering passwords cancelled. Try again?")" 9 78; }; then

				reset
				echo
				echo "hooks/install.DEFAULT.sh: Installation cancelled while entering root password."
				echo
				exit 1
			fi

		elif [ "$inp1" == "$inp2" ] ; then
			ROOTPW=`mkpasswd -Hsha-256 "$inp1"`
			echo "Password hash for root set."
			if [ ! -z "${TEST_MODE}" ]; then
				reset
				echo
				echo "hooks/install.DEFAULT.sh: Root password hash is '$ROOTPW'."
				echo
				sleep 2
				break
			fi
		else

			DIALOGRC=${red} \
			dialog --stdout \
			       --title " $(eval_gettext "Password for super-user 'root'") " \
			       --backtitle " $(eval_gettext "Configure Base System") " \
			       --msgbox "\n$(eval_gettext "Passwords do not match, please try again.")" 8 45
		fi
		unset inp1 inp2
	done
fi


#
#  Prompt for the hostname if $HOSTNAME is empty.
#

ValidHostnameRegex="^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$"

if [ -z "${HOSTNAME}" ] || [ ! -z "${TEST_MODE}" ]; then
	HOSTNAME=''
	while [ -z "${HOSTNAME}" ] || [ ! -z "${TEST_MODE}" ]; do
		if ! { inp=`dialog --insecure --stdout --backtitle " $(eval_gettext "Configure Base System") " --title " $(eval_gettext "Hostname Setup") " --inputbox \
			"\n$(eval_gettext "No hostname for this machine could be detected.")\n\n$(eval_gettext "Please enter a hostname:")" 12 78`; } ; then

			if { ! dialog --stdout \
			              --yes-button "$(eval_gettext "Yes")"\
			              --no-button "$(eval_gettext "Cancel installation")" \
			              --title " $(eval_gettext "Hostname Setup") " \
			              --backtitle " $(eval_gettext "Configure Base System") " \
			              --yesno "\n$(eval_gettext "Entering hostname cancelled. Try again?")" 9 78; }; then

				reset
				echo
				echo "hooks/install.DEFAULT.sh: Installation cancelled while entering hostname."
				echo
				exit 1
			fi

		elif [[ $inp =~ $ValidHostnameRegex ]] ; then

			HOSTNAME="$inp"

			if [ -z "${TEST_MODE}" ]; then
				## Make 'hostname' and 'hostname -d' succeed, important for sssd installation:
				hostname "$HOSTNAME"
				sed -i "1 a127.0.1.1       ${HOSTNAME}.intern  ${HOSTNAME}" /etc/hosts
			else
				reset
				echo
				echo "hooks/install.DEFAULT.sh: Hostname set to '$HOSTNAME'."
				echo
				sleep 2
				break
			fi

		else
			DIALOGRC=${red} \
			dialog --stdout \
			       --backtitle " $(eval_gettext "Configure Base System") " \
			       --title " $(eval_gettext "Hostname Setup") " \
			       --msgbox "\n`printf "$(eval_gettext "Hostname '%s' invalid, please try again.") "${inp}"`" 8 45
		fi
		unset inp
	done
fi

set +e
