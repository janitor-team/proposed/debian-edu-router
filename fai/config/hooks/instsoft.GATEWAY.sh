#! /bin/bash

# Copyright (C) 2001-2022 Thomas Lange <lange@informatik.uni-koeln.de>
# Copyright (C) 2022-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
#  Disable services when converting a minimal installation.
#  Create necessary directories if missing.

POLICYFILE="/usr/sbin/policy-rc.d"

## Only when converting:
if [ "$CONVERT" == "true" ] && [ "$target" == "/" ] && [ ! -e $POLICYFILE ] ; then
	cat > $POLICYFILE <<EOF
#!/bin/sh
exit 101
EOF
	chmod a+rx $POLICYFILE
	mkdir -p /var/lib/fai/config

	## for systemd, mask the units:
	ln -fs /dev/null /etc/systemd/system/multi-user.target.wants/isc-dhcp-server.service
else
	unset POLICYFILE
fi

# if etckeeper gets used and etckeeper uses git, then install git now
# and pre-configure it before etckeeper installation
install_packages -l 2>/dev/null | egrep  -q  ' etckeeper '
with_etckeeper=$?
install_packages -l 2>/dev/null | egrep  -q  ' git '
with_git=$?
if [ $with_git -eq 0 ] && [ $with_etckeeper -eq 0 ]; then
    if [ X$verbose = X1 ]; then
	$ROOTCMD apt-get -y install git
    else
	$ROOTCMD apt-get -y install git > /dev/null
    fi
    $ROOTCMD git config --global init.defaultBranch main
fi
