#! /bin/bash

# Copyright (C) 2001-2022 Thomas Lange <lange@informatik.uni-koeln.de>
# Copyright (C) 2022-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# parse all log files for error messages
# print errors and warnings found to error.log
# WARNING: This will only work with english error messages!

errfile=$LOGDIR/error.log

# Define grep patterns. Do not start or end with an empty line!
globalerrorpatterns="error
fail
warn
 bad
bad 
no space
syntax
Couldn't stat
Cannot access
 conflict
is bigger than the limit
did not exist
non existent
not found
couldn't
can't
E: Sorry, broken packages
^E: 
operator expected
ambiguous redirect
No previous regular expression
No such
Device or resource busy
unknown option
[a-z]\+\.log:E:
No candidate version found
segfault
Couldn't find any package whose name or description matched
cannot create
The following packages have unmet dependencies"

globalignorepatterns="[a-z]\+\.log:#
Error: Driver 'pcspkr' is already registered, aborting
: bytes  packets  errors  dropped
:+ error=0
:+ trap error=
task_error_func=
STOP_ON_ERROR=
courier-webadmin
plugins-bad
Enabling conf localized-error-pages
ibwebadmin
kernel-patch-badram
kolab-webadmin
kolabadmin
gstreamer.\+-plugins-really-bad
liberrors.so
liberrors-samba
libsamba-errors
gsambad
libad
libtest-nowarnings-perl
libtest-warn-perl
libclass-errorhandler-perl
zope-ploneerrorreporting
libroxen-errormessage
liberror-perl
libgpg-error-dev
libgpg-error0
Opts:.\+errors=remount
[RT]X packets:
WARNING: unexpected IO-APIC
warned about = ( )
daemon.warn
kern.warn
rw,errors=
Expect some cache
no error
failmsg
RPC call returned error 101
deverror.out
(floppy), sector 0
mount version older than kernel
Can't locate module
Warning only .\+MB will be used.
hostname: Host name lookup failure
I can't tell the difference.
warning, not much extra random data, consider using the -rand option
confC._FILE
Warning: 3 database(s) sources
were not found, (but were created)
removing exim
The home dir you specified already exists.
No Rule for /usr/lib/ispell/default.hash.
/usr/sbin/update-fonts-.\+: warning: absolute path
hostname: Unknown server error
EXT2-fs warning: checktime reached
RPC: sendmsg returned error 101
can't print them to stdout. Define these classes
warning: downgrading
suppress emacs errors
echo Error:
Can't open dependencies file
documents in /usr/doc are no longer supported
if you have both a SCSI and an IDE CD-ROM
Warning: /proc/ide/hd?/settings interface is obsolete, and will be removed soon
Monitoring disabled
Error: only one processor found.
Error Recovery Strategy:
sector 0 does not have an
syslogin_perform_logout: logout() returned an error
grub is not in an XFS filesystem.
grub-install: line 374:
grub-probe: error: Cannot open \`/boot/grub/device.map'
is harmless
not updating .\+ font directory data.
register_serial(): autoconfig failed
Fontconfig error: Cannot load default config file
asking for cache data failed
However, I can not read the target:
Warning: The partition table looks like it was made
task_error=0
task_local_error=0
^info: Trying to set
warning: /usr/lib/X11/fonts
can't read /etc/udev/rules.d/z25_persistent-net.rules
/cow': No such file or directory
Dummy start-stop-daemon called
X: bytes  packets  errors
ACPI Error
ACPI Warning
AE_NOT_FOUND
conflicts with ACPI region
cannot stat \`/etc/modprobe.d/\*.conf'
cdrom: open failed.
libgpg-error
process \`kudzu' used the deprecated sysctl system call
PM: Resume from disk failed
JBD: barrier-based sync failed
aufs: module is from the staging directory, the quality is unknown
warning: linuxlogo stop runlevel arguments (none) do not match
insserv: warning: script .\+ missing LSB tags and overrides
live-premount.\+ If this fails
cannot read table of mounted file systems
error: no alternatives for
ERST: Error Record Serialization Table (ERST) support is initialized
ERST: Table is not found
HEST: Table not found
failed to stat /dev/pts
Failed to connect to socket /var/run/dbus/system_bus_socket
fail to add MMCONFIG information
can't initialize iptables table
can't initialize ip6tables table
Authentication warning overridden
41-warning.sh
PCCT header not found
Download is performed unsandboxed as root as file
update-alternatives: warning: skip creation of
loop: module verification failed: signature
Warning: apt-key output should not be parsed
WARNING: Failed to connect to lvmetad. Falling back to device scanning
Warning: The home dir /var/lib/usbmux you specified
diff: /var/lib/apparmor/profiles/.apparmor.md5sums: No such file or directory
error reporting disabled
Enabling Firmware First mode for corrected errors
errors: 0
 0 errors
Memory Error Correction:
Memory Controller 0 - Channel . Error
IIO RAS/Control Status/Global Errors
__stack_chk_fail
update-rc.d: warning: start and stop actions are no longer supported"

# add pattern on some conditions
if [ -n $FAI_ALLOW_UNSIGNED ] ; then
   globalignorepatterns="$globalignorepatterns
WARNING: untrusted versions
WARNING: The following packages cannot be authenticated
Ignoring these trust violations"
fi
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Here you can define your own patterns. Put one pattern in a line,
# do not create empty lines.
myerrorpatterns="X_X-X_XX"
myignorepatterns="create mode
Warning: you may need to reload your webservice!
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
libpng warning: iCCP: known incorrect sRGB profile
Warning: The home directory 
Warning: found usr.sbin.sssd in
Warning failed to create cache: usr.sbin.sssd
error changing net interface name
could not rename interface
head: cannot open
update-inetd: warning: cannot add service"
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# The main routine
errorpatterns="$globalerrorpatterns
$myerrorpatterns"
ignorepatterns="$globalignorepatterns
$myignorepatterns"

cd $LOGDIR || exit 3
if [ -s $errfile ]; then
	echo "Errorfile already exists. Aborting." >&2
	exit
fi

grep -i "$errorpatterns" *.log | grep -vi "$ignorepatterns" > $errfile
if [ X$verbose = X1 ]; then
	egrep -v '^software.log:' $errfile > $LOGDIR/tempfile
	mv $LOGDIR/tempfile $errfile
fi

if [ -s $errfile ]; then
	echo "ERRORS reported in ${errfile}:"
	nl $errfile
	echo "ERRORS found in log files. See above and $errfile" >&2
else
	echo "Congratulations! No errors found in log files."
fi
