#!/bin/bash

# Copyright (C) 2011-2018 Andreas B. Mundt <andi@debian.org>
# Copyright (C) 2022-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# Enable services again when converting a minimal installation.
# Only do that when converting.

if [ -n "$POLICYFILE" ] ; then
	rm $POLICYFILE

	## unmask the units again:
	systemctl unmask isc-dhcp-server
fi

#
# Run etckeeper:
#

$ROOTCMD etckeeper commit "saving modifications after configure scripts run" || true
