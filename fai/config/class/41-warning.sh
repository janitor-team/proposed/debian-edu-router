#! /bin/bash

# Copyright (C) 2001-2022 Thomas Lange <lange@informatik.uni-koeln.de>
# Copyright (C) 2022-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

if [ X$FAI_ACTION = Xinstall -o X$FAI_ACTION = X ]; then
    :
else
    return 0
fi
if [ X$action = Xdirinstall ]; then
    return 0
fi

grep -q INSTALL $LOGDIR/FAI_CLASSES || return 0
[ "$flag_menu" ] || return 0

out=$(tty)
red=$(mktemp)
echo 'screen_color = (CYAN,RED,ON)' > $red

DIALOGRC=$red dialog --colors --clear --aspect 6 --title "FAI - Fully Automatic Installation" --trim \
	        --msgbox "\n\n        If you continue,       \n   all your data on the disk   \n                               \n|\Zr\Z1       WILL BE DESTROYED     \Z0\Zn|\n\n" 0 0 1>$out

# stop on any error, or if ESC was hit
if [ $? -ne 0 ]; then
    task_error 999
fi

rm $red
unset red
