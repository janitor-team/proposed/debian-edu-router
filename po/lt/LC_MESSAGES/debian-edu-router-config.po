# Lithuanian translations for PACKAGE package.
# Copyright (C) 2022-2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-27 15:28+0100\n"
"PO-Revision-Date: 2022-04-20 15:46+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n"
"%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../bin/debian-edu-router-loginmenu.sh:36
#, sh-format
msgid "Welcome to %s %s (%s) on %s"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:37
#, sh-format
msgid "WARNING: All networks are currently down. Check your configuration."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:38
#, sh-format
msgid "machine ID"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:40
#, sh-format
msgid "%s Menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:41
#: ../bin/debian-edu-router-loginmenu.sh:54
#, sh-format
msgid "%s Configuration"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:42
#, sh-format
msgid "Launch a shell session"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:43
#, sh-format
msgid "IP traffic statistics"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:44
#, sh-format
msgid "Reboot system"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:45
#, sh-format
msgid "Shutdown system"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:46
#, sh-format
msgid "Quit menu and logout as user '%s'"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:47
#, sh-format
msgid "Please select: "
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:49
#, sh-format
msgid "Starting IPTRAF traffic monitor..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:50
#, sh-format
msgid "Starting shell '%s'..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:51
#, sh-format
msgid "Entering %s configuration submenu..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:52
#, sh-format
msgid "Quit"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:55
#, sh-format
msgid "Configure %s network settings"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:56
#, sh-format
msgid "Configure %s firewall settings"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:57
#, sh-format
msgid "Configure %s services"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:59
#, sh-format
msgid "Configure networking..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:60
#, sh-format
msgid "Configure firewall settings..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:61
#, sh-format
msgid "Configure services..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:62
#, sh-format
msgid "Back to main menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:64
#, sh-format
msgid "Return back to main menu"
msgstr ""

#. Reboot stuff
#: ../bin/debian-edu-router-loginmenu.sh:67
#, sh-format
msgid "Are you sure you want to reboot the system? Type in 'imsure':"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:68
#, sh-format
msgid ""
"WARNING!\n"
"Make sure you still have (physical) access to the machine in case the boot\n"
"process fails. If this is a VM Guest, make sure you have access to the\n"
"VM Host. An internet downage can easily ruin your day!\n"
"You have been warned."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:73
#: ../bin/debian-edu-router-loginmenu.sh:84
#, sh-format
msgid "imsure"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:75
#, sh-format
msgid "OK. Rebooting %s in %s seconds!"
msgstr ""

#. Shutdown stuff
#: ../bin/debian-edu-router-loginmenu.sh:78
#, sh-format
msgid "Are you sure you want to shut down the system? Type in 'imsure':"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:79
#, sh-format
msgid ""
"WARNING!\n"
"Make sure you still have (physical) access to the machine!\n"
"If this is a VM Guest, make sure you have access to the\n"
"VM Host. An internet downage can easily ruin your day!\n"
"You have been warned."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:86
#, sh-format
msgid "OK. Shutting down %s in %s seconds!"
msgstr ""
