# Spanish translations for PACKAGE package.
# Copyright (C) 2022-2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-27 15:28+0100\n"
"PO-Revision-Date: 2022-04-20 15:46+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../fai/config/hooks/install.GATEWAY.sh:77
#: ../fai/config/hooks/install.GATEWAY.sh:95
#: ../fai/config/hooks/install.GATEWAY.sh:117
#: ../fai/config/hooks/install.GATEWAY.sh:136
#: ../fai/config/hooks/install.GATEWAY.sh:145
#: ../fai/config/hooks/install.GATEWAY.sh:155
#: ../fai/config/hooks/install.GATEWAY.sh:164
#: ../fai/config/hooks/install.GATEWAY.sh:174
#: ../fai/config/hooks/install.GATEWAY.sh:185
#: ../fai/config/hooks/install.GATEWAY.sh:199
#: ../fai/config/hooks/install.GATEWAY.sh:230
#: ../fai/config/hooks/install.GATEWAY.sh:233
#: ../fai/config/hooks/install.GATEWAY.sh:239
#, sh-format
msgid "Configure Internet Access"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:78
#, sh-format
msgid "No network adapter connected!"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:79
#: ../fai/config/hooks/install.GATEWAY.sh:241
#, sh-format
msgid "Try again"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:79
#: ../fai/config/hooks/install.GATEWAY.sh:117
#: ../fai/config/hooks/install.GATEWAY.sh:241
#: ../fai/config/hooks/install.DEFAULT.sh:48
#: ../fai/config/hooks/install.DEFAULT.sh:98
#, sh-format
msgid "Cancel installation"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:80
#: ../fai/config/hooks/install.GATEWAY.sh:98
#, sh-format
msgid ""
"Please plugin a network cable to the uplink network interface and try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:96
#, sh-format
msgid "Please choose uplink [external] network interface"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:97
#, sh-format
msgid "Select"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:97
#, sh-format
msgid "Re-scan network interfaces"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:117
#: ../fai/config/hooks/install.GATEWAY.sh:137
#, sh-format
msgid "External IPv4 address"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:118
#, sh-format
msgid "Please specify the external IPv4 address of this system."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:138
#: ../fai/config/hooks/install.GATEWAY.sh:176
#, sh-format
msgid "IPv4 address '%s' is of invalid format, please try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:145
#: ../fai/config/hooks/install.GATEWAY.sh:156
#, sh-format
msgid "External IPv4 netmask"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:145
#: ../fai/config/hooks/install.GATEWAY.sh:164
#: ../fai/config/hooks/install.GATEWAY.sh:185
#, sh-format
msgid "Back"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:146
#, sh-format
msgid "Please specify the external interface's IPv4 netmask."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:157
#, sh-format
msgid "IPv4 netmask '%s' is of invalid format, please try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:164
#: ../fai/config/hooks/install.GATEWAY.sh:175
#, sh-format
msgid "External IPv4 gateway"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:165
#, sh-format
msgid "Please specify the external interface's IPv4 gateway address."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:185
#: ../fai/config/hooks/install.GATEWAY.sh:200
#, sh-format
msgid "Upstream DNS servers"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:186
#, sh-format
msgid ""
"Please specify available IPv4 DNS servers on the external network. Use "
"commas or blanks to separate several DNS server addresses."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:201
#, sh-format
msgid "DNS server's IPv4 address '%s' is of invalid format, please try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:230
#: ../fai/config/hooks/install.GATEWAY.sh:233
#, sh-format
msgid "Manual IPv4 network setup [required]"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:231
#, sh-format
msgid ""
"Internet access still fails using the IPv4 networking data you entered "
"manually."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:231
#, sh-format
msgid "Do you want to retry setting up IPv4 internet access manually?"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:234
#, sh-format
msgid "The system failed to obtain an IPv4 address via DHCP automatically."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:234
#, sh-format
msgid "Do you want to setup IPv4 internet access manually?"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:240
#, sh-format
msgid "No IPv4 network / internet access available!"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:243
#, sh-format
msgid "Fix this problem [Ctrl-Alt-F2] and try again."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:41
#: ../fai/config/hooks/install.DEFAULT.sh:49
#: ../fai/config/hooks/install.DEFAULT.sh:75
#, sh-format
msgid "Password for super-user 'root'"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:41
#: ../fai/config/hooks/install.DEFAULT.sh:43
#: ../fai/config/hooks/install.DEFAULT.sh:50
#: ../fai/config/hooks/install.DEFAULT.sh:76
#: ../fai/config/hooks/install.DEFAULT.sh:93
#: ../fai/config/hooks/install.DEFAULT.sh:100
#: ../fai/config/hooks/install.DEFAULT.sh:130
#, sh-format
msgid "Configure Base System"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:42
#, sh-format
msgid ""
"Please provide a secure password for the super-user 'root' (system "
"administrator account)."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:42
#, sh-format
msgid "Please enter the root password:"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:43
#, sh-format
msgid "Password for super-user 'root' [again]"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:44
#, sh-format
msgid "Please re-enter the same password for the super-user 'root' account."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:44
#, sh-format
msgid "Please enter the root password again:"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:47
#: ../fai/config/hooks/install.DEFAULT.sh:97
#, sh-format
msgid "Yes"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:51
#, sh-format
msgid "Entering passwords cancelled. Try again?"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:77
#, sh-format
msgid "Passwords do not match, please try again."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:93
#: ../fai/config/hooks/install.DEFAULT.sh:99
#: ../fai/config/hooks/install.DEFAULT.sh:131
#, sh-format
msgid "Hostname Setup"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:94
#, sh-format
msgid "No hostname for this machine could be detected."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:94
#, sh-format
msgid "Please enter a hostname:"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:101
#, sh-format
msgid "Entering hostname cancelled. Try again?"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:132
#, sh-format
msgid "Hostname '%s' invalid, please try again."
msgstr ""
