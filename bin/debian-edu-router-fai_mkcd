#!/bin/sh

# Copyright (C) 2010-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# Create ISO image / SquashFS image for installation of Debian Edu Router.

set -ex

LC_ALL=C
export LC_ALL

# Make sure the created directories and files are readable by tfptd
# run as user nobody.
umask 022

mode="$(echo $(basename $0) | sed -e "s/debian-edu-router-fai_mk//")"

###
### FAI Setup
###

[ "$archs" ]            || archs=$(command -V dpkg 1>/dev/null 2>/dev/null && dpkg --print-architecture || uname -m)
[ "$codenames" ]        || codenames=$(cat /etc/os-release | grep VERSION_CODENAME | cut -d "=" -f2)

# required for pre-selecting the default boot item in iPXE config
[ "$default_arch" ]     || default_arch="$(echo ${archs} | cut -d " " -f1)"
[ "$default_codename" ] || default_codename=$(echo ${codenames} | cut -d " " -f1)

# source debian-edu-fai's config file
# Allow site specific overrides to the variables
if [ -f /etc/debian-edu/debian-edu-router-fai.conf ] ; then
	. /etc/debian-edu/debian-edu-router-fai.conf
fi

for codename in ${codenames}; do

	# skip codenames that don't sound like Debian suites...
	if ! echo "bullseye bookworm sid unstable" | grep -q "${codename}"; then
		echo "WARNING: The name '${codename}' is not a known and recent Debian distribution codename. Skipping..."
		continue
	fi

	# iterate over configured FAI client architectures...
	for arch in ${archs}; do

		# create codename based fai base config
		faiconfig="/etc/debian-edu/fai/debian-edu-router-fai.${arch}+${codename}"
		if [ ! -d /etc/debian-edu/fai/debian-edu-router-fai.${arch}+${codename} ]; then
			echo "ERROR: Failed to find FAI configuration in ${faiconfig}, no debian-edu-router-fai.${arch}+${codename} directory found"
			echo "HINT:  Run debian-edu-router-faiinstall first."
			exit 1
		fi

		# source the NFS conf file... this might override our FAI_CONFIGDIR
		# (but should not as recommended in our nfsroot.conf.in template)
		if [ -f "$faiconfig/nfsroot.conf" ]; then
			. $faiconfig/nfsroot.conf
		else
			echo "ERROR: No nfsroot.conf file found in $faiconfig/, can't continue..."
			exit 1
		fi

		# hard-code some sensible defaults in case they have been commented out in $faiconfig/nfsroot.conf
		[ "$NFSROOT" ]              || NFSROOT="/srv/fai/nfsroot.debian-edu-router-fai/${arch}+${codename}"

		# the NFSROOT variable we should have obtained from sourcing $faiconfig/nfsroot.conf
		# (aka /etc/fai/nfsroot.conf) above...
		if [ -n "${NFSROOT}" ] && [ -n "${codename}" ]; then

			# create nfs-root from scratch (if not present or not fully created in a previous run)

			# Create a ".DEBIAN_EDU_FAI_NFSROOT_INSTALLATION_COMPLETED" file at the end
			# of fai-make-nfsroot and check for the presence of that file for detecting
			# whether a fresh NFSROOT setup is required or just an NFSROOT update/upgrade.
			if [ ! -f "${NFSROOT}/.DEBIAN_EDU_FAI_NFSROOT_INSTALLATION_COMPLETED" ]; then

				echo "ERROR: Can't find ${NFSROOT}/.DEBIAN_EDU_FAI_NFSROOT_INSTALLATION_COMPLETED"
				echo "ERROR: FAI nfsroot installation is not fully complete."
				echo "HINT:  Re-run 'debian-edu-router-faiinstall' and look for errors."
				exit 1

			else

				# adjust nfsroot configuration (SSH pubkeys, rootpw, etc.)
				fai-make-nfsroot -v -a -C ${faiconfig}

				if [ "$mode" = "cd" ]; then
					# create ISO image file (USB/DVD/CD installer)
					mkdir -p /var/lib/debian-edu-router-fai/iso-images/
					fai-cd -f -C ${faiconfig} -M /var/lib/debian-edu-router-fai/iso-images/.debian-edu-router.${arch}+${codename}.iso.tmp
					# create backup of previous ISO image
					if [ -e "/var/lib/debian-edu-router-fai/iso-images/debian-edu-router.${arch}+${codename}.iso" ]; then
						mv /var/lib/debian-edu-router-fai/iso-images/debian-edu-router.${arch}+${codename}.iso /var/lib/debian-edu-router-fai/iso-images/debian-edu-router.${arch}+${codename}.iso.old
					fi
					# "activate" new ISO image (activate as in "make visible")
					if [ -e "/var/lib/debian-edu-router-fai/iso-images/.debian-edu-router.${arch}+${codename}.iso.tmp" ]; then
						mv /var/lib/debian-edu-router-fai/iso-images/.debian-edu-router.${arch}+${codename}.iso.tmp /var/lib/debian-edu-router-fai/iso-images/debian-edu-router.${arch}+${codename}.iso
					fi
				elif [ "$mode" = "image" ]; then
					# create SquashFS image file
					mkdir -p /var/lib/debian-edu-router-fai/squashfs-images/
					fai-cd -f -C ${faiconfig} -M -S /var/lib/debian-edu-router-fai/squashfs-images/.debian-edu-router.${arch}+${codename}.img.tmp
					# create backup of previous SquashFS image
					if [ -e "/var/lib/debian-edu-router-fai/squashfs-images/debian-edu-router.${arch}+${codename}.img" ]; then
						mv /var/lib/debian-edu-router-fai/squashfs-images/debian-edu-router.${arch}+${codename}.img /var/lib/debian-edu-router-fai/squashfs-images/debian-edu-router.${arch}+${codename}.img.old
					fi
					# "activate" new SquashFS image (activate as in "make visible")
					if [ -e "/var/lib/debian-edu-router-fai/squashfs-images/.debian-edu-router.${arch}+${codename}.img.tmp" ]; then
						mv /var/lib/debian-edu-router-fai/squashfs-images/.debian-edu-router.${arch}+${codename}.img.tmp /var/lib/debian-edu-router-fai/squashfs-images/debian-edu-router.${arch}+${codename}.img
					fi
				else
					echo "ERROR: Script mode is unknown. This script needs to be launched as debian-edu-faicd"
					echo "ERROR: or debian-edu-faiimage."
					exit 1
				fi

			fi

		fi

	done
done
